﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using Newtonsoft.Json;

namespace StickyNotes
{
    public class ApplicationManager
    {
        public HashSet<Note> OpenedNotes { get; private set; }
        public GlobalSettings settings;
        
        public void Initialize()
        {
            OpenedNotes = new HashSet<Note>();

            LoadSettings();
            LoadNotes();
        }

        #region Loading data
        public string LoadSettings(bool loadDefault = false)
        {
            string dataToLoad = string.Empty;
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

            try
            {
                dataToLoad = File.ReadAllText(Path.Combine(baseDirectory, @"data\json\globalSettingsUser.json"));
            }
            catch (Exception)
            {
                MessageBox.Show("Saved global settings are corrupted. The default settings are loaded instead.", "Confirmation", MessageBoxButton.OK);

                dataToLoad = File.ReadAllText(Path.Combine(baseDirectory, @"data\json\globalSettingsDefault.json"));
            }
            finally
            {
                settings = JsonConvert.DeserializeObject<GlobalSettings>(dataToLoad);
            }
            return dataToLoad;
        }

        private void LoadNotes()
        {
            string dataToLoad = string.Empty;
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

            try
            {
                dataToLoad = File.ReadAllText(Path.Combine(baseDirectory, @"data\json\savedNotes.json"));
            }
            catch (Exception)
            {
                MessageBox.Show("Your data is corrupted.", "Confirmation", MessageBoxButton.OK);

                dataToLoad = File.ReadAllText(Path.Combine(baseDirectory, @"data\json\globalSettingsDefault.json"));
            }
            finally
            {
                StickyNotesCollection snCol = JsonConvert.DeserializeObject<StickyNotesCollection>(dataToLoad);

                foreach (var note in snCol.loadedNotes)
                {
                    LoadNote(note);
                }
            }
        }

        private void LoadNote(StickyNote note)
        {
            Note createdNote = new Note(note, this);
            OpenedNotes.Add(createdNote);
            createdNote.UpdateNoteView();
            createdNote.Show();
        }
        #endregion

        #region Saving data
        private void SaveNotes(GlobalSettings updatedSettings = null)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

            StickyNotesCollection collection = new StickyNotesCollection(OpenedNotes.Count);
            int i = 0;

            foreach (var noteWindow in OpenedNotes)
            {
                noteWindow.UpdateNoteData();
                collection.loadedNotes[i] = noteWindow.NoteData;
                i++;
            }
            string dataToSave = JsonConvert.SerializeObject(collection, Formatting.Indented);
            
            File.WriteAllText(Path.Combine(baseDirectory, @"data\json\savedNotes.json"), dataToSave);
        }

        public string SaveSettings(GlobalSettings updatedSettings = null)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string settingsToSave = string.Empty;
            if (updatedSettings != null)
            {
                settingsToSave = JsonConvert.SerializeObject(updatedSettings, Formatting.Indented);
                File.WriteAllText(Path.Combine(baseDirectory, @"data\json\globalSettingsUser.json"), settingsToSave);
            }
            return settingsToSave;
        }
        #endregion

        public void RemoveNote(Note note)
        {
            OpenedNotes.Remove(note);
            note.Close();
        }

        #region Main window taskbar functionalities
        public Note AddNote()
        {
            StickyNote noteData = new StickyNote();
            noteData.positionX = settings.positionX;
            noteData.positionY = settings.positionY;
            noteData.sizeX = settings.scaleX;
            noteData.sizeY = settings.scaleY;
            noteData.fontSize = settings.fontSize;
            noteData.isBold = settings.isBold;
            noteData.isItalic = settings.isItalic;
            noteData.content = string.Empty;
            noteData.colorBackground = settings.colorNote;
            noteData.colorForeground = settings.colorFont;
            noteData.dateCreated = DateTime.Now;
            noteData.dateLastModified = DateTime.Now;

            Note createdNote = new Note(noteData, this);
            createdNote.UpdateNoteView();
            OpenedNotes.Add(createdNote);

            createdNote.Show();

            return createdNote;
        }

        public SettingsWindow OpenSettings()
        {
            SettingsWindow settings = new SettingsWindow(this);
            settings.Show();
            return settings;
        }

        public void HideApplicationWindows()
        {
            foreach (var w in OpenedNotes)
            {
                w.WindowState = WindowState.Minimized;
            }
        }

        public void ShowApplicationWindows()
        {
            foreach (var w in OpenedNotes)
            {
                w.WindowState = WindowState.Normal;
            }
        }

        public void CloseApplication()
        {
            SaveNotes();
            SaveSettings();

            foreach (var w in OpenedNotes)
                w.Close();
        }
#endregion
    }
}
