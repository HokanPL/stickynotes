﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace StickyNotes
{
    /// <summary>
    /// Logika interakcji dla klasy Settings.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        private Dictionary<string, string> colorHexCodes;
        private ApplicationManager manager;

        public SettingsWindow(ApplicationManager appManager)
        {
            manager = appManager;

            colorHexCodes = new Dictionary<string, string>();
            colorHexCodes["violet"] = "#9C00A6";
            colorHexCodes["pink"] = "#F000FF";
            colorHexCodes["red"] = "#FF0000";
            colorHexCodes["orange"] = "#FF9700";
            colorHexCodes["yellow"] = "#FFFF00";
            colorHexCodes["lime"] = "#00FF00";
            colorHexCodes["green"] = "#008000";
            colorHexCodes["silver"] = "#C0C0C0";
            colorHexCodes["grey"] = "#808080";
            colorHexCodes["black"] = "#000000";

            InitializeComponent();
            LoadData();
        }

        private void LoadData()
        {
            BoldChecked.IsChecked = manager.settings.isBold;
            ItalicChecked.IsChecked = manager.settings.isItalic;
            TextSize.Text = manager.settings.fontSize.ToString();

            foreach (var item in colorHexCodes)
            {
                if (item.Value == manager.settings.colorFont)
                    TextColorChoice.SelectedItem = TextColorChoice.FindName(item.Key + "_font");

                if (item.Value == manager.settings.colorNote)
                    BackgroundColorChoice.SelectedItem = BackgroundColorChoice.FindName(item.Key + "_back");
            }

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            GlobalSettings settingsModified = new GlobalSettings();

            settingsModified.positionX = 300;
            settingsModified.positionY = 200;
            settingsModified.scaleX = 250;
            settingsModified.scaleY = 300;

            settingsModified.isBold = (bool)BoldChecked.IsChecked;
            settingsModified.isItalic = (bool)ItalicChecked.IsChecked;
            settingsModified.fontSize = Math.Min(Math.Max(10, int.Parse(TextSize.Text)), 40);


            string keyColorFont = TextColorChoice.SelectedItem.ToString();
            int found = keyColorFont.IndexOf(": ");
            settingsModified.colorFont = colorHexCodes[keyColorFont.Substring(found + 2)];

            string keyNoteFont = BackgroundColorChoice.SelectedItem.ToString();
            found = keyNoteFont.IndexOf(": ");
            settingsModified.colorNote = colorHexCodes[keyNoteFont.Substring(found + 2)];

            manager.settings = settingsModified;

            Close();
        }

        private void TextSize_FocusLost(object sender, RoutedEventArgs e)
        {
            int targetSize = Math.Min(Math.Max(10, int.Parse(TextSize.Text)), 40);
            TextSize.Text = targetSize.ToString();
        }
    }
}
