﻿using System;

namespace StickyNotes
{
    public class StickyNote
    {
        public int positionX;
        public int positionY;
        public int sizeX;
        public int sizeY;

        public string content;
        public string colorBackground;
        public string colorForeground;
        public int fontSize;
        public bool isBold;
        public bool isItalic;
        
        public DateTime dateCreated;
        public DateTime dateLastModified;
    }

    class StickyNotesCollection
    {
        public StickyNote[] loadedNotes;

        public StickyNotesCollection(int numberOfNotes)
        {
            loadedNotes = new StickyNote[numberOfNotes];
        }
    }
}
