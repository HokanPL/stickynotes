﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace StickyNotes
{
    public partial class Note : Window
    {
        public StickyNote NoteData { get; private set; }
        public ApplicationManager applicationManager;

        public Note(StickyNote note, ApplicationManager appManager)
        {
            InitializeComponent();
            NoteData = note;
            applicationManager = appManager;

            this.MouseDown += Note_MouseDown;
        }

        private void Note_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        public void UpdateNoteData()
        {
            var bc = new BrushConverter();

            NoteData.positionX = (int)Left;
            NoteData.positionY = (int)Top;
            NoteData.sizeX = (int)Width;
            NoteData.sizeY = (int)Height;
            NoteData.content = noteTextField.Text;
            NoteData.colorBackground = (Background as SolidColorBrush).Color.ToString();
            NoteData.colorForeground = (noteTextField.Foreground as SolidColorBrush).Color.ToString();
            NoteData.fontSize = (int)noteTextField.FontSize;
            NoteData.isItalic = (noteTextField.FontStyle == FontStyles.Italic);
            NoteData.isBold = (noteTextField.FontWeight == FontWeights.Bold);
        }

        public void UpdateNoteView()
        {
            var bc = new BrushConverter();
            Background = (Brush)bc.ConvertFrom(NoteData.colorBackground);
            Left = NoteData.positionX;
            Top = NoteData.positionY;
            Width = NoteData.sizeX;
            Height = NoteData.sizeY;
            noteTextField.Text = NoteData.content;
            noteTextField.Foreground = (Brush)bc.ConvertFrom(NoteData.colorForeground);
            noteTextField.FontSize = NoteData.fontSize;
            noteTextField.FontStyle = (NoteData.isItalic) ? FontStyles.Italic : FontStyles.Normal;
            noteTextField.FontWeight = (NoteData.isBold) ? FontWeights.Bold : FontWeights.Normal;
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            applicationManager.RemoveNote(this);
        }

        private void Italic_Click(object sender, RoutedEventArgs e)
        {
            NoteData.isItalic = !NoteData.isItalic;
            UpdateTransform();
            UpdateNoteView();
        }

        private void Bold_Click(object sender, RoutedEventArgs e)
        {
            NoteData.isBold = !NoteData.isBold;
            UpdateTransform();
            UpdateNoteView();
        }

        private void Increase_Click(object sender, RoutedEventArgs e)
        {
            NoteData.fontSize = Math.Min(40, NoteData.fontSize + 2);
            UpdateTransform();
            UpdateNoteView();
        }

        private void Decrease_Click(object sender, RoutedEventArgs e)
        {
            NoteData.fontSize = Math.Max(10, NoteData.fontSize - 2);
            UpdateTransform();
            UpdateNoteView();
        }

        private void UpdateTransform()
        {
            NoteData.positionX = (int)Left;
            NoteData.positionY = (int)Top;
            NoteData.sizeX = (int)Width;
            NoteData.sizeY = (int)Height;
        }
    }
}
