﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StickyNotes.Tests
{
    [TestClass()]
    public class ApplicationManagerTests
    {
        [TestMethod()]
        public void is_application_manager_initialized()
        {
            ApplicationManager manager = new ApplicationManager();
            manager.Initialize();

            Assert.IsNotNull(manager.settings);
            Assert.IsNotNull(manager.OpenedNotes);
        }

        [TestMethod()]
        public void are_user_settings_saved_properly()
        {
            ApplicationManager manager = new ApplicationManager();
            manager.Initialize();

            //load default data from test 
            string jsonLoaded = manager.LoadSettings(true);

            //serialize loaded data back to json
            string jsonSaved = manager.SaveSettings(manager.settings);

            Assert.AreEqual(jsonLoaded, jsonSaved);
        }

        [TestMethod()]
        public void is_note_added_to_opened_windows()
        {
            ApplicationManager manager = new ApplicationManager();
            manager.Initialize();

            //number of opened windows is relative to number of notes
            int numberOfWindowsOpenedBefore = manager.OpenedNotes.Count;
            manager.AddNote();
            int numberOfWindowsOpenedAfter = manager.OpenedNotes.Count;

            Assert.AreEqual(numberOfWindowsOpenedAfter, numberOfWindowsOpenedBefore + 1);
        }

        [TestMethod()]
        public void is_note_removed_from_opened_windows()
        {
            ApplicationManager manager = new ApplicationManager();
            manager.Initialize();
            Note n = manager.AddNote();
            
            //number of opened windows is relative to number of notes
            int numberOfWindowsOpenedBefore = manager.OpenedNotes.Count;
            manager.RemoveNote(n);
            int numberOfWindowsOpenedAfter = manager.OpenedNotes.Count;

            Assert.AreEqual(numberOfWindowsOpenedAfter, numberOfWindowsOpenedBefore - 1);
        }

        [TestMethod()]
        public void can_open_settings_window()
        {
            ApplicationManager manager = new ApplicationManager();
            manager.Initialize();

            SettingsWindow window = manager.OpenSettings();

            Assert.IsNotNull(window);
        }
    }
}